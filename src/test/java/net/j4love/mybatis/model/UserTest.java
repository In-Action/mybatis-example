package net.j4love.mybatis.model;

import static org.junit.Assert.*;

/**
 * @author he peng
 * @create 2017/11/7 16:51
 * @see
 */
public class UserTest {

    public void changeUser1(User user) {
        user.setId(6666L);
        user = null;
    }

    public void changeUser2(User user) {
        user.setId(88888L);
        user = null;
    }

    public static void main(String[] args) {
        User user = new User();
        user.setId(123L);

        System.out.println("before : " + user);
        UserTest test = new UserTest();

        test.changeUser1(user);
        System.out.println("changeUser1 : " + user);

        test.changeUser2(user);
        System.out.println("changeUser2 : " + user);

    }
}