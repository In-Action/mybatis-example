package redis;

import net.j4love.workx.spring.boot.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author he peng
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class RedisClusterTest {

    @Autowired
    RedisTemplate redisTemplate;

    @Test
    public void set() {
        redisTemplate.opsForValue().set("cluster-fail" , "ASVVSAFSF");
    }

    @Test
    public void get() {
        Object val = redisTemplate.opsForValue().get("cluster-fail");
        System.out.println(val);
    }
}
