package net.j4love.jdbc;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Properties;

/**
 * @author he peng
 * @create 2017/11/10 15:08
 * @see
 */
public class PrepareStatementTest {

    private static final Logger LOG = LoggerFactory.getLogger(PrepareStatementTest.class);
    private static final Properties PROP = new Properties();
    private static final int COUNT = 2000000;

    private String driverClass;
    private String url;
    private String user;
    private String password;

    private static final StringBuilder TEST_PREPAREED_STATEMENT_SQL = new StringBuilder();
    private long startTime;

    @Before
    public void init() throws Exception {

        PROP.load(PrepareStatementTest.class.getClassLoader().getResourceAsStream("jdbc.properties"));
        this.driverClass = PROP.getProperty("jdbc.driver");
        this.url = PROP.getProperty("jdbc.url");
        this.user = PROP.getProperty("jdbc.user");
        this.password = PROP.getProperty("jdbc.password");
        DriverManager.registerDriver((Driver) Class.forName(this.driverClass).newInstance());

        TEST_PREPAREED_STATEMENT_SQL.append("INSERT INTO `user` (nickname,realname,phone,login_password,pay_password,create_time,update_time) VALUES");
        for (int i = 0 ; i < COUNT ; i++ ) {
            if (i == COUNT - 1) {
                TEST_PREPAREED_STATEMENT_SQL.append(" (?,?,?,?,?,?,?)");
            } else {
                TEST_PREPAREED_STATEMENT_SQL.append(" (?,?,?,?,?,?,?)").append(", ");
            }
        }

        this.startTime = System.currentTimeMillis();
    }

    @After
    public void after() throws Exception {
        LOG.debug("======> cost time {} ms" , (System.currentTimeMillis() - this.startTime));
    }

    @Test
    public void costTime() throws Exception {
        Connection connection = DriverManager.getConnection(url, user, password);
        PreparedStatement ps = connection.prepareStatement(TEST_PREPAREED_STATEMENT_SQL.toString());

    }
}
