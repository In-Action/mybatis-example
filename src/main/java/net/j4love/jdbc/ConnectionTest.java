package net.j4love.jdbc;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.util.Properties;

/**
 * @author he peng
 * @create 2017/11/10 16:21
 * @see
 */
public class ConnectionTest {

    private static final Logger LOG = LoggerFactory.getLogger(ConnectionTest.class);
    private static final Properties PROP = new Properties();
    private static final int COUNT = 4000;

    private String driverClass;
    private String url;
    private String user;
    private String password;

    private long startTime;

    @Before
    public void init() throws Exception {

        PROP.load(PrepareStatementTest.class.getClassLoader().getResourceAsStream("jdbc.properties"));
        this.driverClass = PROP.getProperty("jdbc.driver");
        this.url = PROP.getProperty("jdbc.url");
        this.user = PROP.getProperty("jdbc.user");
        this.password = PROP.getProperty("jdbc.password");
        DriverManager.registerDriver((Driver) Class.forName(this.driverClass).newInstance());

        this.startTime = System.currentTimeMillis();
    }

    @After
    public void after() throws Exception {
        LOG.debug("======> cost time {} ms" , (System.currentTimeMillis() - this.startTime));
    }

    @Test
    public void openManyConnection() throws Exception {
        for (int i = 0 ;i < COUNT ; i++ ) {
            DriverManager.getConnection(url, user, password);
        }
    }
}
