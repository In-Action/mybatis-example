package net.j4love.mybatis.example;

import net.j4love.mybatis.dao.UserDAO;
import net.j4love.mybatis.model.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Date;

/**
 * @author He Peng
 * @create 2017-11-12 0:22
 * @update 2017-11-12 0:22
 * @updatedesc : 更新说明
 * @see
 */
public class ExecutorTest {

    private static final Logger LOG = LoggerFactory.getLogger(SelectByPrimaryKeyStatementTest.class);

    private SqlSessionFactory sqlSessionFactory;

    @Before
    public void init() throws Exception {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        this.sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }

    @Test
    public void simple() throws Exception {
        SqlSession session = this.sqlSessionFactory.openSession(ExecutorType.SIMPLE);
        UserDAO userDAO = session.getMapper(UserDAO.class);
        userDAO.selectCount();
    }

    @Test
    public void reuse() throws Exception {
        SqlSession session = this.sqlSessionFactory.openSession(ExecutorType.REUSE);
        UserDAO userDAO = session.getMapper(UserDAO.class);
        userDAO.selectCount();
    }

    @Test
    public void simpleAutoCommit() throws Exception {
        User user = new User();
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setNickname("simple-Auto-Commit");
        user.setRealname("杰森伯恩");
        user.setPhone(13723236323L);
        user.setLoginPassword("123456789");
        user.setPayPassword("123456");

        SqlSession session = this.sqlSessionFactory.openSession(ExecutorType.SIMPLE,true);
        UserDAO userDAO = session.getMapper(UserDAO.class);
        userDAO.insert(user);
    }

}
