package net.j4love.mybatis.example;

import net.j4love.mybatis.dao.UserDAO;
import net.j4love.mybatis.model.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

/**
 * @author He Peng
 * @create 2017-06-22 23:19
 * @update 2017-06-22 23:19
 * @updatedesc : 更新说明
 * @see
 */
public class QuickStart {

    public static void main(String[] args) throws IOException {

        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory =
                new SqlSessionFactoryBuilder().build(inputStream);

        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        User user = new User();
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setNickname("jason-D");
        user.setRealname("杰森伯恩");
        user.setPhone(13723236323L);
        user.setLoginPassword("123456789");
        user.setPayPassword("123456");

        String namespace = UserDAO.class.getName();
        sqlSession.insert(namespace + "." + "insert",user);

//        Object obj = sqlSession.selectOne(namespace + "." + "selectByPrimaryKey", user);
//        System.out.println(obj);

    }
}
