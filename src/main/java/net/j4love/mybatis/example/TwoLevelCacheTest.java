package net.j4love.mybatis.example;

import net.j4love.mybatis.dao.DepartmentDAO;
import net.j4love.mybatis.dao.EmployeeDAO;
import net.j4love.mybatis.model.Department;
import net.j4love.mybatis.model.Employee;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

/**
 * @author he peng
 * @create 2017/11/23 19:25
 * @see
 */
public class TwoLevelCacheTest {

    private static final Logger LOG = LoggerFactory.getLogger(TwoLevelCacheTest.class);

    private SqlSessionFactory sqlSessionFactory;

    @Before
    public void init() throws Exception {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        this.sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }

    @Test
    public void onlySingleSessionSelect() throws Exception {
        SqlSession sqlSession = this.sqlSessionFactory.openSession(true);
        EmployeeDAO employeeDAO = sqlSession.getMapper(EmployeeDAO.class);
        Employee employee0 = employeeDAO.selectByPrimaryKey(1L);

        LOG.info("first select ==> {} " , employee0);

        Employee employee1 = employeeDAO.selectByPrimaryKey(1L);

        LOG.info("second select ==> {} " , employee1);

    }

    @Test
    public void multiSession() throws Exception {
        SqlSession sqlSession0 = this.sqlSessionFactory.openSession();
        EmployeeDAO employeeDAO0 = sqlSession0.getMapper(EmployeeDAO.class);
        Employee emp0 = employeeDAO0.selectByPrimaryKey(1L);
        sqlSession0.commit();
        LOG.info("session 1 select ==> {} " , emp0);

        SqlSession sqlSession1 = this.sqlSessionFactory.openSession();
        EmployeeDAO employeeDAO1 = sqlSession1.getMapper(EmployeeDAO.class);
        Employee emp1 = employeeDAO1.selectByPrimaryKey(1L);

        LOG.info("session 2 select ==> {} " , emp1);

        SqlSession sqlSession2 = this.sqlSessionFactory.openSession();
        EmployeeDAO employeeDAO2 = sqlSession2.getMapper(EmployeeDAO.class);
        Employee emp = new Employee();
        emp.setId(1L).setName("杰森斯坦森");
        int i = employeeDAO2.updateByPrimaryKeySelective(emp);
        sqlSession2.commit();
        LOG.info("session 3 update ==> {} " , i);

        Employee emp2 = employeeDAO1.selectByPrimaryKey(1L);

        LOG.info("session 2 select ==> {} " , emp2);

    }

    @Test
    public void diffMapper() throws Exception {
        SqlSession sqlSession = this.sqlSessionFactory.openSession();
        EmployeeDAO employeeDAO = sqlSession.getMapper(EmployeeDAO.class);
        Employee emp = employeeDAO.selectByPrimaryKey(1L);
        sqlSession.commit();

        LOG.info("EmployeeDAO session select ==> {} " , emp);

        SqlSession sqlSession1 = this.sqlSessionFactory.openSession();
        DepartmentDAO departmentDAO = sqlSession1.getMapper(DepartmentDAO.class);
        Department dept = departmentDAO.selectByPrimaryKey(1L);

        LOG.info("DepartmentDAO session select ==> {} " , dept);

    }
}
