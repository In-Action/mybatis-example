package net.j4love.mybatis.example;

import net.j4love.mybatis.dao.DepartmentDAO;
import net.j4love.mybatis.dao.EmployeeDAO;
import net.j4love.mybatis.dao.UserDAO;
import net.j4love.mybatis.dto.UserQueryDTO;
import net.j4love.mybatis.model.Department;
import net.j4love.mybatis.model.Employee;
import net.j4love.mybatis.model.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.List;

/**
 * @author he peng
 * @create 2017/11/7 11:51
 * @see
 */
public class SelectByPrimaryKeyStatementTest {

    private static final Logger LOG = LoggerFactory.getLogger(SelectByPrimaryKeyStatementTest.class);

    private SqlSessionFactory sqlSessionFactory;

    @Before
    public void init() throws Exception {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        this.sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }

    @Test
    public void multipleResultSetsEnabled() throws Exception {
        SqlSession sqlSession = this.sqlSessionFactory.openSession(true);
        UserDAO userDAO = sqlSession.getMapper(UserDAO.class);
        User user = userDAO.getUserByPhone(13723236323L);
        System.out.println(user);
    }

    @Test
    public void resultMapCollection() throws Exception {
        SqlSession sqlSession = this.sqlSessionFactory.openSession(true);
        UserDAO userDAO = sqlSession.getMapper(UserDAO.class);
        User user = userDAO.getUserWithInterestById(1L);
        System.out.println(user);
    }

    @Test
    public void lazySelectOneToMany() throws Exception {
        SqlSession sqlSession = this.sqlSessionFactory.openSession(true);
        DepartmentDAO depDAO = sqlSession.getMapper(DepartmentDAO.class);
        Department dept = depDAO.selectByPrimaryKey(2L);

        LOG.debug("result {} " , dept);
    }

    @Test
    public void lazySelectOneToOne() throws Exception {
        SqlSession sqlSession = this.sqlSessionFactory.openSession(true);
        EmployeeDAO empDAO = sqlSession.getMapper(EmployeeDAO.class);
        Employee emp = empDAO.selectByPrimaryKey(1L);

        Department department = emp.getDepartment();
    }

    @Test
    public void dynamicSqlTest() throws Exception {
        SqlSession sqlSession = this.sqlSessionFactory.openSession(true);
        UserDAO userDAO = sqlSession.getMapper(UserDAO.class);
        UserQueryDTO query = new UserQueryDTO();
        query.setNickname("jason-D")
             .setPhone(13723236323L);
        List<User> list = userDAO.selectList(query);
        LOG.debug("result {} " ,list);
    }

    @Test
    public void staticSqlTest() throws Exception {
        SqlSession sqlSession = this.sqlSessionFactory.openSession(true);
        UserDAO userDAO = sqlSession.getMapper(UserDAO.class);
        long count = userDAO.selectCount();
        LOG.debug("count = " + count);
    }

    @Test
    public void getMapperProxy() throws Exception {
        SqlSession sqlSession = this.sqlSessionFactory.openSession(true);
        UserDAO userDAO = sqlSession.getMapper(UserDAO.class);
        LOG.debug("DAO {} , super Class {}" , userDAO , userDAO.getClass().getSuperclass());
    }

    @Test
    public void selectOne() throws Exception {

        // 默认事务不会自动提交
        SqlSession sqlSession = this.sqlSessionFactory.openSession();
        MappedStatement ms = this.sqlSessionFactory.
                getConfiguration().getMappedStatement("net.j4love.mybatis.dao.UserDAO.selectByPrimaryKey");
        LOG.debug("执行的 SQL {} " , ms.getBoundSql(null).getSql());
        Object obj = sqlSession.selectOne(ms.getId(), 1);
        LOG.debug("result {} " , obj);

        Object obj1 = sqlSession.selectOne(ms.getId(), 1);
        LOG.debug("result 1 {} " , obj1);

    }

}
