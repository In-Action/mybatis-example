package net.j4love.mybatis.dao;

import net.j4love.mybatis.model.Interest;

/**
 * @author he peng
 * @create 2017/12/11 13:57
 * @see
 */
public interface InterestDAO {

    int insert(Interest interest);
}
