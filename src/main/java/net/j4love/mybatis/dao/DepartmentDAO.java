package net.j4love.mybatis.dao;

import net.j4love.mybatis.model.Department;

/**
 * @author He Peng
 * @create 2017-11-13 22:21
 * @update 2017-11-13 22:21
 * @updatedesc : 更新说明
 * @see
 */
public interface DepartmentDAO {

    int insert(Department department);

    Department selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Department department);
}
