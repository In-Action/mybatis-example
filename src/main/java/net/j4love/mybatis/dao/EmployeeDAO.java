package net.j4love.mybatis.dao;

import net.j4love.mybatis.model.Employee;

import java.util.List;

/**
 * @author He Peng
 * @create 2017-11-13 22:19
 * @update 2017-11-13 22:19
 * @updatedesc : 更新说明
 * @see
 */
public interface EmployeeDAO {

    int insert(Employee emp);

    Employee selectByPrimaryKey(Long id);

    List<Employee> selectListByDeptId(Long deptId);

    int updateByPrimaryKeySelective(Employee emp);

}
