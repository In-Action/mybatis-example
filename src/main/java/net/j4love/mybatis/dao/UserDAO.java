package net.j4love.mybatis.dao;


import net.j4love.mybatis.dto.UserQueryDTO;
import net.j4love.mybatis.model.User;
import net.j4love.workx.mybatis.InsertEventConsumer0;
import net.j4love.workx.mybatis.listener.UserDAOQueryEventConsumer;
import org.hepeng.workx.jdbc.DataSourceRoute;
import org.hepeng.workx.mybatis.event.consumer.EventConsumerBind;
import org.hepeng.workx.mybatis.mapper.Mapper;
import org.hepeng.workx.mybatis.mapper.SimplifyCodingSupport;

import java.util.List;

/**
 * @author He Peng
 * @create 2017-06-22 23:31
 * @update 2017-06-22 23:31
 * @updatedesc : 更新说明
 * @see
 */

@SimplifyCodingSupport(entityClass = User.class)
@EventConsumerBind({UserDAOQueryEventConsumer.class , InsertEventConsumer0.class})
public interface UserDAO extends Mapper<User> {

//    @DataSourceRoute({"dataSource1"})
//    int insert(User user);

//    Integer insertSelective(User user);

    User selectByPrimaryKey(Long id);

    long selectCount();

    @DataSourceRoute({"test0"})
    List<User> selectList(UserQueryDTO query);

    int insertBatch(List<User> users);

    int updateNicknameByPrimaryKey(User user);

    User getUserWithInterestById(Long id);

    User getUserByPhone(Long phone);

}
