package net.j4love.mybatis.dto;

/**
 * @author He Peng
 * @create 2017-11-07 22:25
 * @update 2017-11-07 22:25
 * @updatedesc : 更新说明
 * @see
 */
public class UserQueryDTO {

    private String nickname;
    private String realname;
    private Long phone;
    private String startCreateTime;
    private String endCreateTime;

    public String getNickname() {
        return nickname;
    }

    public UserQueryDTO setNickname(String nickname) {
        this.nickname = nickname;
        return this;
    }

    public String getRealname() {
        return realname;
    }

    public UserQueryDTO setRealname(String realname) {
        this.realname = realname;
        return this;
    }

    public Long getPhone() {
        return phone;
    }

    public UserQueryDTO setPhone(Long phone) {
        this.phone = phone;
        return this;
    }

    public String getStartCreateTime() {
        return startCreateTime;
    }

    public UserQueryDTO setStartCreateTime(String startCreateTime) {
        this.startCreateTime = startCreateTime;
        return this;
    }

    public String getEndCreateTime() {
        return endCreateTime;
    }

    public UserQueryDTO setEndCreateTime(String endCreateTime) {
        this.endCreateTime = endCreateTime;
        return this;
    }

    @Override
    public String toString() {
        return "UserQueryDTO{" +
                "nickname='" + nickname + '\'' +
                ", realname='" + realname + '\'' +
                ", phone=" + phone +
                ", startCreateTime='" + startCreateTime + '\'' +
                ", endCreateTime='" + endCreateTime + '\'' +
                '}';
    }
}
