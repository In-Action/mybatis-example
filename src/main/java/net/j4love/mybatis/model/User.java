package net.j4love.mybatis.model;

import org.hepeng.workx.mybatis.mapper.Column;
import org.hepeng.workx.mybatis.mapper.Id;
import org.hepeng.workx.mybatis.mapper.LogicDelete;
import org.hepeng.workx.mybatis.mapper.TableMap;

import java.util.Date;
import java.util.List;

/**
 * @author He Peng
 * @create 2017-06-22 23:31
 * @update 2017-06-22 23:31
 * @updatedesc : 更新说明
 * @see
 */

@TableMap(tableName = "user")
public class User {

    @Id("id")
    private Long id;

//    @Id("idd")
//    private Long idd;

    @Column("nickname")
    private String nickname;

    @Column("realname")
    private String realname;

    @Column("phone")
    private Long phone;

    @Column("login_password")
    private String loginPassword;

    @Column("pay_password")
    private String payPassword;

    @Column("create_time")
    private Date createTime;

    @Column("update_time")
    private Date updateTime;

    @Column("deleted")
    @LogicDelete(notDeleted = "0" , deleted = "1")
    private Byte deleted;

    private List<Interest> interests;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    public String getPayPassword() {
        return payPassword;
    }

    public void setPayPassword(String payPassword) {
        this.payPassword = payPassword;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public List<Interest> getInterests() {
        return interests;
    }

    public User setInterests(List<Interest> interests) {
        this.interests = interests;
        return this;
    }

    public Byte getDeleted() {
        return deleted;
    }

    public void setDeleted(Byte deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", nickname='" + nickname + '\'' +
                ", realname='" + realname + '\'' +
                ", phone=" + phone +
                ", loginPassword='" + loginPassword + '\'' +
                ", payPassword='" + payPassword + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", interests=" + interests +
                '}';
    }
}
