package net.j4love.mybatis.model;

import java.util.Date;

/**
 * @author he peng
 * @create 2017/12/11 13:56
 * @see
 */
public class Interest {

    private Long id;
    private Long userId;
    private String category;
    private String describe;
    private Date createTime;

    public Long getId() {
        return id;
    }

    public Interest setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getUserId() {
        return userId;
    }

    public Interest setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public String getCategory() {
        return category;
    }

    public Interest setCategory(String category) {
        this.category = category;
        return this;
    }

    public String getDescribe() {
        return describe;
    }

    public Interest setDescribe(String describe) {
        this.describe = describe;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Interest setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    @Override
    public String toString() {
        return "Interest{" +
                "id=" + id +
                ", userId=" + userId +
                ", category='" + category + '\'' +
                ", describe='" + describe + '\'' +
                ", createTime=" + createTime +
                '}';
    }
}
