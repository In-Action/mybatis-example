package net.j4love.mybatis.model;

import java.util.Date;

/**
 * @author He Peng
 * @create 2017-11-13 22:16
 * @update 2017-11-13 22:16
 * @updatedesc : 更新说明
 * @see
 */
public class Employee {

    private Long id;
    private Long deptId;
    private String name;
    private Integer age;
    private Date createTime;
    private Date updateTime;
    private Department department;

    public Long getId() {
        return id;
    }

    public Employee setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getDeptId() {
        return deptId;
    }

    public Employee setDeptId(Long deptId) {
        this.deptId = deptId;
        return this;
    }

    public String getName() {
        return name;
    }

    public Employee setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getAge() {
        return age;
    }

    public Employee setAge(Integer age) {
        this.age = age;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Employee setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public Employee setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public Department getDepartment() {
        return department;
    }

    public Employee setDepartment(Department department) {
        this.department = department;
        return this;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", deptId=" + deptId +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", department=" + department +
                '}';
    }
}
