package net.j4love.mybatis.model;

import java.util.Date;
import java.util.List;

/**
 * @author He Peng
 * @create 2017-11-13 22:19
 * @update 2017-11-13 22:19
 * @updatedesc : 更新说明
 * @see
 */
public class Department {

    private Long id;
    private String name;
    private String addr;
    private Date createTime;
    private Date updateTime;
    private List<Employee> emps;

    public Long getId() {
        return id;
    }

    public Department setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Department setName(String name) {
        this.name = name;
        return this;
    }

    public String getAddr() {
        return addr;
    }

    public Department setAddr(String addr) {
        this.addr = addr;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Department setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public Department setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public List<Employee> getEmps() {
        return emps;
    }

    public Department setEmps(List<Employee> emps) {
        this.emps = emps;
        return this;
    }

    @Override
    public String toString() {
        return "Department{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", addr='" + addr + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", emps=" + emps +
                '}';
    }
}
