package net.j4love.mybatis.pagehelper;

import com.github.pagehelper.PageHelper;
import net.j4love.mybatis.dao.UserDAO;
import net.j4love.mybatis.dto.UserQueryDTO;
import net.j4love.mybatis.example.UpdateStatementTest;
import net.j4love.mybatis.model.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.List;

/**
 * @author he peng
 * @create 2017/11/17 20:05
 * @see
 */
public class PageInterceptorTest {

    private static final Logger LOG = LoggerFactory.getLogger(PageInterceptorTest.class);

    private SqlSessionFactory sqlSessionFactory;

    private long startTime;


    @Before
    public void init() throws Exception {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        this.sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        this.startTime = System.currentTimeMillis();
    }

    @After
    public void after() throws Exception {
        LOG.trace("======> cost time = {} ms" , (System.currentTimeMillis() - this.startTime));
    }

    @Test
    public void pageInterceptor() throws Exception {
        SqlSession sqlSession = this.sqlSessionFactory.openSession();
        UserDAO userDAO = sqlSession.getMapper(UserDAO.class);
        PageHelper.startPage(100000,30);
        UserQueryDTO query = new UserQueryDTO();
        query.setPhone(13723236323L);
        List<User> users = userDAO.selectList(query);

        LOG.info("result ==> {}" , users);
    }
}
