package net.j4love.workx.mybatis;

import net.j4love.mybatis.dao.UserDAO;
import net.j4love.mybatis.model.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.hepeng.workx.mybatis.session.SqlSessionFactoryBuilderX;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.Date;

/**
 * @author he peng
 */
public class EventPublishExecutorTest {

    private SqlSessionFactory sqlSessionFactory;

    @Before
    public void init() throws Exception {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        this.sqlSessionFactory = new SqlSessionFactoryBuilderX().build(inputStream);
    }

    @Test
    public void test0() throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        User user = new User();
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setNickname("jason-D");
        user.setRealname("杰森伯恩");
        user.setPhone(13723236323L);
        user.setLoginPassword("123456789");
        user.setPayPassword("123456");

        String namespace = UserDAO.class.getName();
        int insert = sqlSession.insert(namespace + "." + "insert", user);
        System.out.println(insert);
        Thread.sleep(60000);
    }
}
