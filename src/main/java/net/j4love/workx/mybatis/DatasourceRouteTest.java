package net.j4love.workx.mybatis;

import net.j4love.mybatis.dao.UserDAO;
import net.j4love.mybatis.dto.UserQueryDTO;
import net.j4love.mybatis.model.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.hepeng.workx.mybatis.session.SqlSessionFactoryBuilderX;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

/**
 * @author he peng
 */
public class DatasourceRouteTest {

    private SqlSessionFactory sqlSessionFactory;

    @Before
    public void init() throws Exception {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        this.sqlSessionFactory = new SqlSessionFactoryBuilderX().build(inputStream);
    }

    @Test
    public void test0() throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);

        User user = new User();
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setNickname("jason-D");
        user.setRealname("杰森伯恩");
        user.setPhone(13723236323L);
        user.setLoginPassword("123456789");
        user.setPayPassword("123456");
        UserDAO mapper = sqlSession.getMapper(UserDAO.class);
        mapper.insert(user);
    }

    @Test
    public void test1() throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);

        UserDAO mapper = sqlSession.getMapper(UserDAO.class);
        UserQueryDTO query = new UserQueryDTO();
        query.setRealname("杰森伯恩");
        List<User> users = mapper.selectList(query);
//        Thread.sleep(80000);
    }
}
