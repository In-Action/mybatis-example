package net.j4love.workx.mybatis;

import org.hepeng.workx.constant.ExecuteMode;
import org.hepeng.workx.mybatis.event.consumer.EventConsumer;
import org.hepeng.workx.mybatis.event.InsertEvent;
import org.hepeng.workx.mybatis.event.consumer.MyBatisEventConsumer;

/**
 * @author he peng
 */

@MyBatisEventConsumer(order = 100)
public class InsertEventConsumer2 implements EventConsumer<InsertEvent> {

    @Override
    public void onConsume(InsertEvent insertEvent) {
        System.out.println("InsertEventConsume2 on consume -> " + insertEvent);
    }

    @Override
    public void onError(Throwable error) {
        System.out.println("InsertEventConsumer2 on error -> " + error);
    }

    @Override
    public void onComplete() {
        System.out.println("InsertEventConsumer2 on complete -> ");
    }

    @Override
    public ExecuteMode getExecuteMode() {
        return ExecuteMode.ASYNC;
    }

    @Override
    public int order() {
        return -1;
    }
}
