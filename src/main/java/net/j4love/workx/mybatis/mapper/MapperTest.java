package net.j4love.workx.mybatis.mapper;

import net.j4love.mybatis.dao.UserDAO;
import net.j4love.mybatis.model.User;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.hepeng.workx.mybatis.session.SqlSessionFactoryBuilderX;
import org.hepeng.workx.util.PageQuerys;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

/**
 * @author he peng
 */
public class MapperTest {

    private SqlSessionFactory sqlSessionFactory;

    @Before
    public void init() throws Exception {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        this.sqlSessionFactory = new SqlSessionFactoryBuilderX().build(inputStream);
    }

    @Test
    public void insertSelective() throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        User user = new User();
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setNickname("jason-D");
        user.setRealname("杰森伯恩");
        user.setPhone(13723236323L);
        user.setLoginPassword("123456789");
        user.setPayPassword("123456");

        UserDAO mapper = sqlSession.getMapper(UserDAO.class);
        mapper.insertSelective(user);
    }

    @Test
    public void updateById() throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        User user = new User();
        user.setId(1L);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setNickname("XSA");
        user.setRealname("杰森");
        user.setPhone(13723236323L);
        user.setLoginPassword("4324fdfsf");
        user.setPayPassword("2ffffffffffffffffff");

        UserDAO mapper = sqlSession.getMapper(UserDAO.class);
        mapper.updateById(user);
    }

    @Test
    public void updateSelectiveById() throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        User user = new User();
        user.setId(1L);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setNickname("WQXXAGDFGDFG");
        user.setRealname("杰森");

        UserDAO mapper = sqlSession.getMapper(UserDAO.class);
        mapper.updateSelectiveById(user);
    }

    @Test
    public void deleteByIdAtPhysical() throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        UserDAO mapper = sqlSession.getMapper(UserDAO.class);
        Integer integer = mapper.deleteByIdAtPhysical(1L);
        System.out.println(integer);
    }

    @Test
    public void deleteByIdAtLogic() throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        UserDAO mapper = sqlSession.getMapper(UserDAO.class);
        Integer integer = mapper.deleteByIdAtLogic(1L);
        System.out.println(integer);
    }

    @Test
    public void selectById() throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        UserDAO mapper = sqlSession.getMapper(UserDAO.class);
        User user = mapper.selectOneById(1L);
        System.out.println(user);
    }

    @Test
    public void selectByCondition() throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        UserDAO mapper = sqlSession.getMapper(UserDAO.class);

        User query = new User();
        query.setNickname("jason-D");
        query.setRealname("杰森伯恩");

        PageQuerys.markQueryPage(0L , 12);
        List<User> users = mapper.selectByCondition(query);
        System.out.println(users);
    }

    @Test
    public void se() {
        char c = "select user.update_time as updateTime, user.deleted as deleted, user.login_password as loginPassword, user.create_time as createTime, user.phone as phone, user.pay_password as payPassword, user.nickname as nickname, user.id as id, user.realname as realname from user   <where>  <\"if test1=\"updateTime!= null\">\n".charAt(279);
        System.out.println(c);
    }
}
