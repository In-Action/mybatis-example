package net.j4love.workx.mybatis;

import org.hepeng.workx.constant.ExecuteMode;
import org.hepeng.workx.mybatis.event.consumer.EventConsumer;
import org.hepeng.workx.mybatis.event.consumer.MyBatisEventConsumer;
import org.hepeng.workx.mybatis.event.QueryEvent;
import org.hepeng.workx.mybatis.executor.SQLExecuteContext;

/**
 * @author he peng
 */

@MyBatisEventConsumer(order = 626)
public class QueryEventConsumer0 implements EventConsumer<QueryEvent> {

    @Override
    public void onConsume(QueryEvent queryEvent) {
        SQLExecuteContext context = queryEvent.getSQLExecuteContext();
        System.out.println("QueryEventConsumer0 on consume -> " + queryEvent);
    }

    @Override
    public void onError(Throwable error) {
        System.out.println("QueryEventConsumer0 on error -> " + error);
    }

    @Override
    public void onComplete() {
        System.out.println("QueryEventConsumer0 on complete -> ");
    }

    @Override
    public ExecuteMode getExecuteMode() {
        return ExecuteMode.ASYNC;
    }

    @Override
    public int order() {
        return -1;
    }
}
