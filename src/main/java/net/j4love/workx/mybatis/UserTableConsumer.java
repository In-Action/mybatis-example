package net.j4love.workx.mybatis;

import org.hepeng.workx.constant.ExecuteMode;
import org.hepeng.workx.mybatis.event.consumer.EventConsumer;
import org.hepeng.workx.mybatis.event.consumer.MyBatisEventConsumer;

/**
 * @author he peng
 */

@MyBatisEventConsumer(order = 626)
public class UserTableConsumer implements EventConsumer {

    @Override
    public void onConsume(Object o) {
        System.out.println("UserTableConsumer on consume -> " + o);
    }

    @Override
    public void onError(Throwable error) {
        System.out.println("UserTableConsumer on error -> " + error);
    }

    @Override
    public void onComplete() {
        System.out.println("UserTableConsumer on complete -> ");
    }

    @Override
    public ExecuteMode getExecuteMode() {
        return ExecuteMode.ASYNC;
    }

    @Override
    public int order() {
        return -1;
    }
}
