package net.j4love.workx.mybatis.listener;

import com.google.common.collect.Sets;
import net.j4love.mybatis.dao.UserDAO;
import net.j4love.mybatis.dto.UserQueryDTO;
import net.j4love.mybatis.model.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.hepeng.workx.mybatis.event.listener.ExecuteEventListener;
import org.hepeng.workx.mybatis.session.SqlSessionFactoryBuilderX;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;
import java.util.Set;

/**
 * @author he peng
 */
public class EventConsumerIsolationTest {

    private SqlSessionFactory sqlSessionFactory;

    @Before
    public void init() throws Exception {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);

        Set<ExecuteEventListener> listeners = Sets.newHashSet();
        listeners.add(new EventListener0(true ,"net.j4love.workx.mybatis.listener"));
        this.sqlSessionFactory = new SqlSessionFactoryBuilderX()
                .executeEventListeners(listeners)
                .build(inputStream);
    }

    @Test
    public void test1() throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);

        System.out.println(sqlSession.getMapper(UserDAO.class));
        UserDAO mapper = sqlSession.getMapper(UserDAO.class);
        UserQueryDTO query = new UserQueryDTO();
        query.setRealname("杰森伯恩");
        List<User> users = mapper.selectList(query);
        Thread.sleep(80000);
    }
}
