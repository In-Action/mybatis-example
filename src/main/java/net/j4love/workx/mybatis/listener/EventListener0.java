package net.j4love.workx.mybatis.listener;

import org.hepeng.workx.mybatis.event.listener.AbstractExecuteEventListener;
import org.hepeng.workx.mybatis.event.ExecuteEvent;

/**
 * @author he peng
 */
public class EventListener0 extends AbstractExecuteEventListener {

    public EventListener0(boolean isOnlySpecifiedPackage, String packageNames) {
        super(isOnlySpecifiedPackage, packageNames);
    }

    @Override
    public void onExecuteEvent(ExecuteEvent event) {
        System.err.println("net.j4love.workx.mybatis.listener.EventListener0 ----------------------->");
        super.onExecuteEvent(event);
    }
}
