package net.j4love.workx.spring.boot;

import org.hepeng.workx.service.ServiceCallResult;
import org.hepeng.workx.service.error.Error;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author he peng
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class ExceptionTranslatorTest {

    @Autowired
    ExceptionTranslatorTestBean exceptionTranslatorTestBean;

    @Test
    public void test() {
//        try {
//
//            ServiceCallResult<Error, Object> scr = exceptionTranslatorTestBean.test1();
//        } catch (Throwable t) {
//
//        }

        ServiceCallResult<Error, Object> scr = exceptionTranslatorTestBean.test();
        if (scr.isSuccess()) {
            // ...
        }
        Assert.assertNotNull(scr);
    }
}
