package net.j4love.workx.spring.boot;

import org.hepeng.workx.exception.translate.ExceptionTranslate;
import org.hepeng.workx.exception.translate.SCRSpringJDBCExceptionTranslator;
import org.hepeng.workx.service.error.Error;
import org.hepeng.workx.service.ServiceCallResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author he peng
 */

@Service
public class ExceptionTranslatorTestBean {


    @Transactional(rollbackFor = Throwable.class)
    @ExceptionTranslate(translators = SCRSpringJDBCExceptionTranslator.class)
    public ServiceCallResult<Error, Object> test() {
        if (1 > 0) {
            throw new RuntimeException("SSSSSSSSSSSSSSSSSSSS");
        }
        return null;
    }
}
