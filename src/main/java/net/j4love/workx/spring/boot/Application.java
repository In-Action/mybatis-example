package net.j4love.workx.spring.boot;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.hepeng.workx.exception.translate.EnableExceptionTranslate;
import org.hepeng.workx.exception.translate.RSCRSpringWebLayerExceptionTranslator;
import org.hepeng.workx.exception.translate.SCRJdkExceptionTranslator;
import org.hepeng.workx.exception.translate.SCRSpringJDBCExceptionTranslator;
import org.hepeng.workx.extension.XLoader;
import org.hepeng.workx.service.ServiceCallResult;
import org.hepeng.workx.service.error.Error;
import org.hepeng.workx.util.proxy.ProxyFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.annotation.MapperScans;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author he peng
 */


@SpringBootApplication(scanBasePackages = {"net.j4love.workx.spring.tx" , "net.j4love.workx.spring" , ""})
@MapperScans({@MapperScan(basePackages = "net.j4love.mybatis.dao")})
@EnableExceptionTranslate
@EnableAspectJAutoProxy(exposeProxy = true)
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class , args);
    }

    @Bean
    public SCRJdkExceptionTranslator scrJdkExceptionTranslator() {
        return new SCRJdkExceptionTranslator();
    }

    @Bean
    public SCRSpringJDBCExceptionTranslator scrSpringJDBCExceptionTranslator() {
        return new SCRSpringJDBCExceptionTranslator();
    }


    @RestController
    public static class Controller {

        @Autowired
        ExceptionTranslatorTestBean exceptionTranslatorTestBean;

        @PostMapping("/ex")
        public Map<String , Object> ex() {

            ServiceCallResult<Error, Object> scr = exceptionTranslatorTestBean.test();
            Map<String, Object> map = new HashMap<>();
            map.put("scr" , scr);
            map.put("msg" , "hi!");
            map.put("time" , DateFormatUtils.format(new Date() , "yyyy-MM-dd HH:mm:ss"));
            return map;
        }
    }


}
