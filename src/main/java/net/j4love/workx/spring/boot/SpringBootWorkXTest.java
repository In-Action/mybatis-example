package net.j4love.workx.spring.boot;

import net.j4love.mybatis.dao.UserDAO;
import net.j4love.mybatis.dto.UserQueryDTO;
import net.j4love.mybatis.model.User;
import net.j4love.workx.spring.tx.TxTestService;
import org.hepeng.workx.util.PageQuerys;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

/**
 * @author he peng
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class SpringBootWorkXTest {

    @Autowired
    private UserDAO mapper;

    @Autowired
    private TxTestService txTestService;

    @Test
    public void test0() throws Exception {
        UserQueryDTO query = new UserQueryDTO();
//        query.setRealname("杰森伯恩");
        PageQuerys.markQueryPage(0L , 2);
        List<User> users = mapper.selectList(query);
        PageQuerys.markQueryPage(0L , 3);
        users = mapper.selectList(query);
        System.out.println(users);
//        Thread.sleep(80000);
    }

    @Test
    public void test1() throws Exception {
        User user = new User();
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setNickname("jason-D");
        user.setRealname("杰森伯恩");
        user.setPhone(13723236323L);
        user.setLoginPassword("123456789");
        user.setPayPassword("123456");
        mapper.insertSelective(user);
    }

    @Test
    public void test2() throws Exception {
        txTestService.test1();
        Thread.sleep(5000);
    }
}
