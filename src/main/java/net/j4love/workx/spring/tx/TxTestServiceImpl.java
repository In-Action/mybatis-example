package net.j4love.workx.spring.tx;

import net.j4love.mybatis.dao.UserDAO;
import net.j4love.mybatis.model.User;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @author he peng
 */

@Service
public class TxTestServiceImpl implements TxTestService {

    @Autowired
    private UserDAO userDAO;

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public void test1() {
        User user = new User();
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setNickname("jason-D");
        user.setRealname("杰森伯恩");
        user.setPhone(13723236323L);
        user.setLoginPassword("123456789");
        user.setPayPassword("123456");
        userDAO.insertSelective(user);
//        if (1 > 0) {
//            throw new RuntimeException();
//        }
        TxTestService txTestService = (TxTestService) AopContext.currentProxy();
        txTestService.test2();
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public void test2() {
        System.out.println("test========================== 2");
    }
}
