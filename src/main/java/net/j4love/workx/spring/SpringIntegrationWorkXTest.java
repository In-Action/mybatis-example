package net.j4love.workx.spring;

import net.j4love.mybatis.dao.UserDAO;
import net.j4love.mybatis.dto.UserQueryDTO;
import net.j4love.mybatis.model.User;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Date;

/**
 * @author he peng
 */
public class SpringIntegrationWorkXTest {

    @Test
    public void test0() throws Exception {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:applicationContext-mybatis.xml");
        UserDAO mapper = context.getBean(UserDAO.class);

        UserQueryDTO query = new UserQueryDTO();
        query.setRealname("杰森伯恩");
        mapper.selectList(query);
//        Thread.sleep(80000);
    }
}
