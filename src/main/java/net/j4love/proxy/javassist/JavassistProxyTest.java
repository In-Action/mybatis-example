package net.j4love.proxy.javassist;

import javassist.util.proxy.Proxy;
import javassist.util.proxy.ProxyFactory;
import net.j4love.mybatis.model.User;
import org.junit.Test;

import java.lang.reflect.Constructor;

/**
 * @author he peng
 * @create 2017/11/27 15:12
 * @see
 */
public class JavassistProxyTest {

    @Test
    public void createProxy() throws Exception {

        ProxyFactory enhancer = new ProxyFactory();
        enhancer.setSuperclass(User.class);
        Constructor<User> defaultConstructor = User.class.getConstructor();
        Class<?>[] parameterTypes = defaultConstructor.getParameterTypes();
        Object[] values = new Object[]{};
        Object enhanced = enhancer.create(parameterTypes, values);

        ((Proxy) enhanced).setHandler(new JavassistMethodHandler());

        User user = (User) enhanced;
        user.setNickname("哈哈哈");

        System.err.println("proxy user : " + user);
    }
}
