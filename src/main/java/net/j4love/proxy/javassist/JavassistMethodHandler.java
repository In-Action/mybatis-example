package net.j4love.proxy.javassist;

import javassist.util.proxy.MethodHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;

/**
 * @author he peng
 * @create 2017/11/27 15:20
 * @see
 */
public class JavassistMethodHandler implements MethodHandler {

    private static final Logger LOG = LoggerFactory.getLogger(JavassistMethodHandler.class);

    @Override
    public Object invoke(Object self, Method thisMethod, Method proceed, Object[] args) throws Throwable {
        if (LOG.isDebugEnabled()) {
            LOG.debug("JavassistMethodHandler invoke method is : " + thisMethod.getName());
        }
        return proceed.invoke(self,args);
    }
}
