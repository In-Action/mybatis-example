package net.j4love.proxy.jdk;

/**
 * @author he peng
 * @create 2017/11/9 11:19
 * @see
 */
public interface MyInterface {

    String echo(String s);
}
