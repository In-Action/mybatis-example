package net.j4love.proxy.jdk;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * @author he peng
 * @create 2017/11/9 11:20
 * @see
 */
public class MyJdkProxy<T> implements InvocationHandler {

    private static final Logger LOG = LoggerFactory.getLogger(MyJdkProxy.class);

    private Class<T> proxyInterface;

    public MyJdkProxy(Class<T> proxyInterface) {
        this.proxyInterface = proxyInterface;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        LOG.debug("method {} , args {} " , method , Arrays.deepToString(args));
        return "TEST";
    }

    public Class<T> getProxyInterface() {
        return proxyInterface;
    }

    public MyJdkProxy setProxyInterface(Class<T> proxyInterface) {
        this.proxyInterface = proxyInterface;
        return this;
    }
}
