package net.j4love.proxy.jdk;

import org.junit.Test;

/**
 * @author he peng
 * @create 2017/11/9 11:22
 * @see
 */
public class MyJdkProxyFactoryTest {

    @Test
    public void newInstance() throws Exception {
        MyJdkProxy<MyInterface> jdkProxy = new MyJdkProxy(MyInterface.class);
        MyInterface myInterface = new MyProxyFactory<MyInterface>().newInstance(jdkProxy);
        String s = myInterface.echo("hello");
        System.out.println(s);
    }

    @Test
    public void test() throws Exception {
        MyInterface myInterface = new MyProxyFactory<MyInterface>().newInstance(MyInterface.class);
        String s = myInterface.echo("hello");
        System.out.println(s);
    }
}
