package net.j4love.proxy.jdk;

import java.lang.reflect.Proxy;

/**
 * @author he peng
 * @create 2017/11/9 11:23
 * @see
 */
public class MyProxyFactory<T> {

    public T newInstance(MyJdkProxy<T> myJdkProxy) {
        T instance = (T) Proxy.newProxyInstance(myJdkProxy.getProxyInterface().getClassLoader(),
                new Class[]{myJdkProxy.getProxyInterface()}, myJdkProxy);
        return instance;
    }

    public T newInstance(Class<T> proxyInterface) {
        T instance = (T) Proxy.newProxyInstance(proxyInterface.getClassLoader(),
                new Class[]{proxyInterface}, new MyJdkProxy<T>(proxyInterface));
        return instance;
    }
}
