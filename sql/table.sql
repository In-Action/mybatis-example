CREATE TABLE `user` (
  `id` BIGINT(20) unsigned NOT NULL AUTO_INCREMENT,
  `nickname` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '昵称',
  `realname` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '真实姓名',
  `phone` BIGINT(11) NOT NULL COMMENT '手机号',
  `login_password` VARCHAR(80) NOT NULL COMMENT '登录密码',
  `pay_password` VARCHAR(50) NOT NULL COMMENT '支付密码',
  `create_time` DATETIME NOT NULL COMMENT '创建时间',
  `update_time` DATETIME NOT NULL COMMENT '更新时间',
  `deleted` TINYINT NOT NULL DEFAULT 0 COMMENT '0 - 未删除 , 1 - 已经删除' ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `employee` (
  id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  dept_id BIGINT UNSIGNED NOT NULL COMMENT '所在部门ID' ,
  name VARCHAR(30) NOT NULL COMMENT '姓名' ,
  age TINYINT UNSIGNED NOT NULL COMMENT '年龄' ,
  `create_time` DATETIME NOT NULL COMMENT '创建时间',
  `update_time` DATETIME NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
)ENGINE = InnoDB DEFAULT CHARSET = utf8 COMMENT '员工表';

CREATE TABLE `department` (
  id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  name VARCHAR(30) NOT NULL COMMENT '姓名' ,
  addr VARCHAR(80) NOT NULL COMMENT '部门地址' ,
  `create_time` DATETIME NOT NULL COMMENT '创建时间',
  `update_time` DATETIME NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
)ENGINE = InnoDB DEFAULT CHARSET = utf8 COMMENT '部门表';

CREATE TABLE `interest` (
  id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  user_id BIGINT UNSIGNED NOT NULL COMMENT '用户ID' ,
  category VARCHAR(30) NOT NULL COMMENT '兴趣类型' ,
  `describe` VARCHAR(255) NOT NULL COMMENT '兴趣描述' ,
  create_time DATETIME NOT NULL COMMENT '创建时间' ,
  PRIMARY KEY (`id`)
)ENGINE = InnoDB DEFAULT CHARSET = utf8 COMMENT '兴趣表';
